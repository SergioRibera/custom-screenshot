use std::f32::consts::PI;
use num_traits::{NumCast, ToPrimitive};

use ril::{Rgba, Image, TrueColor, BitPixel};

struct FloatNearest(f32);

// to_i64, to_u64, and to_f64 implicitly affect all other lower conversions.
// Note that to_f64 by default calls to_i64 and thus needs to be overridden.
impl ToPrimitive for FloatNearest {
    // to_{i,u}64 is required, to_{i,u}{8,16} are useful.
    // If a usecase for full 32 bits is found its trivial to add
    fn to_i8(&self) -> Option<i8> {
        self.0.round().to_i8()
    }
    fn to_i16(&self) -> Option<i16> {
        self.0.round().to_i16()
    }
    fn to_i64(&self) -> Option<i64> {
        self.0.round().to_i64()
    }
    fn to_u8(&self) -> Option<u8> {
        self.0.round().to_u8()
    }
    fn to_u16(&self) -> Option<u16> {
        self.0.round().to_u16()
    }
    fn to_u64(&self) -> Option<u64> {
        self.0.round().to_u64()
    }
    fn to_f64(&self) -> Option<f64> {
        self.0.to_f64()
    }
}

pub fn create_blur(
    image: &Image<Rgba>,
    width: u32,
    height: u32,
    sigma: f32,
) -> Image<Rgba> {
    let sigma = if sigma <= 0.0 { 1.0 } else { sigma };

    let tmp = vertical_sample(image, height, 2.0 * sigma, |x| {
        gaussian(x, sigma)
    });

    horizontal_sample(&tmp, width, 2.0 * sigma, |x| {
        gaussian(x, sigma)
    })
}

pub fn gaussian(x: f32, r: f32) -> f32 {
    ((2.0 * PI).sqrt() * r).recip() * (-x.powi(2) / (2.0 * r.powi(2))).exp()
}

pub fn clamp<N>(a: N, min: N, max: N) -> N
where
    N: PartialOrd,
{
    if a < min {
        min
    } else if a > max {
        max
    } else {
        a
    }
}

fn vertical_sample(
    image: &Image<Rgba>,
    new_height: u32,
    support: f32,
    mut filter: impl FnMut(f32) -> f32,
) -> Image<Rgba> {
    let (width, height) = image.dimensions();
    let mut out = Image::new(width, new_height, Rgba::transparent());
    let mut ws = Vec::new();

    let ratio = height as f32 / new_height as f32;
    let sratio = if ratio < 1.0 { 1.0 } else { ratio };
    let src_support = support * sratio;

    for outy in 0..new_height {
        // For an explanation of this algorithm, see the comments
        // in horizontal_sample.
        let inputy = (outy as f32 + 0.5) * ratio;

        let left = (inputy - src_support).floor() as i64;
        let left = clamp(left, 0, <i64 as From<_>>::from(height) - 1) as u32;

        let right = (inputy + src_support).ceil() as i64;
        let right = clamp(
            right,
            <i64 as From<_>>::from(left) + 1,
            <i64 as From<_>>::from(height),
        ) as u32;

        let inputy = inputy - 0.5;

        ws.clear();
        let mut sum = 0.0;
        for i in left..right {
            let w = (filter)((i as f32 - inputy) / sratio);
            ws.push(w);
            sum += w;
        }
        ws.iter_mut().for_each(|w| *w /= sum);

        for x in 0..width {
            let mut t = (0.0, 0.0, 0.0, 0.0);

            for (i, w) in ws.iter().enumerate() {
                let p = image.get_pixel(x, left + i as u32).unwrap();

                let (k1, k2, k3, k4) = p.as_rgba_tuple();
                let vec: (f32, f32, f32, f32) = (
                    NumCast::from(k1).unwrap(),
                    NumCast::from(k2).unwrap(),
                    NumCast::from(k3).unwrap(),
                    NumCast::from(k4).unwrap(),
                );

                t.0 += vec.0 * w;
                t.1 += vec.1 * w;
                t.2 += vec.2 * w;
                t.3 += vec.3 * w;
            }

            // This is not necessarily Rgba.
            let t = Rgba::new(t.0 as u8, t.1 as u8, t.2 as u8, t.3 as u8);
            out.set_pixel(x, outy, t);
        }
    }

    out
}

fn horizontal_sample(
    image: &Image<Rgba>,
    new_width: u32,
    support: f32,
    mut filter: impl FnMut(f32) -> f32,
) -> Image<Rgba>
{
    let (width, height) = image.dimensions();
    let mut out = Image::new(new_width, height, Rgba::transparent());
    let mut ws = Vec::new();

    let max: f32 = f32::MAX;
    let min: f32 = f32::MIN;
    let ratio = width as f32 / new_width as f32;
    let sratio = if ratio < 1.0 { 1.0 } else { ratio };
    let src_support = support * sratio;

    for outx in 0..new_width {
        // Find the point in the input image corresponding to the centre
        // of the current pixel in the output image.
        let inputx = (outx as f32 + 0.5) * ratio;

        // Left and right are slice bounds for the input pixels relevant
        // to the output pixel we are calculating.  Pixel x is relevant
        // if and only if (x >= left) && (x < right).

        // Invariant: 0 <= left < right <= width

        let left = (inputx - src_support).floor() as i64;
        let left = clamp(left, 0, <i64 as From<_>>::from(width) - 1) as u32;

        let right = (inputx + src_support).ceil() as i64;
        let right = clamp(
            right,
            <i64 as From<_>>::from(left) + 1,
            <i64 as From<_>>::from(width),
        ) as u32;

        // Go back to left boundary of pixel, to properly compare with i
        // below, as the kernel treats the centre of a pixel as 0.
        let inputx = inputx - 0.5;

        ws.clear();
        let mut sum = 0.0;
        for i in left..right {
            let w = (filter)((i as f32 - inputx) / sratio);
            ws.push(w);
            sum += w;
        }
        ws.iter_mut().for_each(|w| *w /= sum);

        for y in 0..height {
            let mut t = (0.0, 0.0, 0.0, 0.0);

            for (i, w) in ws.iter().enumerate() {
                let p = image.get_pixel(left + i as u32, y).unwrap();

                let (k1, k2, k3, k4) = p.as_rgba_tuple();

                t.0 += k1 as f32 * w;
                t.1 += k2 as f32 * w;
                t.2 += k3 as f32 * w;
                t.3 += k4 as f32 * w;
            }

            #[allow(deprecated)]
            let t = Rgba::new(
                NumCast::from(FloatNearest(clamp(t.0, min, max))).unwrap(),
                NumCast::from(FloatNearest(clamp(t.1, min, max))).unwrap(),
                NumCast::from(FloatNearest(clamp(t.2, min, max))).unwrap(),
                NumCast::from(FloatNearest(clamp(t.3, min, max))).unwrap(),
            );

            out.set_pixel(outx, y, t);
        }
    }

    out
}
