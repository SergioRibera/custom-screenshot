use std::path::PathBuf;

use clap::{Parser, ValueEnum};

pub fn get_args() -> Args {
    Args::parse()
}

#[derive(Parser, Debug, Default)]
#[command(author, version, about, long_about = None)]
#[command(next_line_help = true)]
pub struct Args {
    #[arg(long, short, default_value = "./")]
    pub out_dir: Option<PathBuf>,
    #[arg(long, short)]
    pub input_file: PathBuf,
    #[arg(long, short, default_value = "png")]
    pub format_image: ArgsImageFormat,

    #[arg(long, default_value = "Monospace")]
    pub font: String,
    #[arg(long, default_value = "12.0")]
    pub font_size: f32,
    #[arg(long, short, default_value = "")]
    pub text: Option<String>,
    #[arg(long, default_value = "bottom")]
    pub text_position: ArgsTextPosition,
    #[arg(long, default_value = "10")]
    pub text_padding: u32,

    #[arg(long, short, default_value = "0")]
    pub border_size: u32,
    #[arg(long, default_value = "#000")]
    pub bg_color: String,
    #[arg(long, default_value = "0")]
    pub bg_size: u32,
    #[arg(long, default_value = "#fff")]
    pub fg_color: String,
    #[arg(long, short, default_value = "0")]
    pub corner_radius: u32,

    #[arg(long, short, default_value = "false")]
    pub shadow: bool,
    #[arg(long, default_value = "0")]
    pub shadow_size: u32,
    #[arg(long)]
    pub shadow_color: Option<String>,
    #[arg(long, default_value = "0.0")]
    pub shadow_radius: f32,
}

#[derive(Clone, Debug, Default, PartialEq, Eq, ValueEnum)]
pub enum ArgsImageFormat {
    Unknown,
    #[default]
    Png,
    Jpeg,
    Gif,
    Bmp,
    Tiff,
    WebP,
}

#[derive(Clone, Debug, Default, PartialEq, Eq, ValueEnum)]
pub enum ArgsTextPosition {
    #[default]
    Top,
    TopLeft,
    TopRight,
    Center,
    CenterLeft,
    CenterRight,
    Bottom,
    BottomLeft,
    BottomRight,
}

impl From<ArgsImageFormat> for ril::ImageFormat {
    fn from(value: ArgsImageFormat) -> Self {
        match value {
            ArgsImageFormat::Unknown => ril::ImageFormat::Unknown,
            ArgsImageFormat::Png => ril::ImageFormat::Png,
            ArgsImageFormat::Jpeg => ril::ImageFormat::Jpeg,
            ArgsImageFormat::Gif => ril::ImageFormat::Gif,
            ArgsImageFormat::Bmp => ril::ImageFormat::Bmp,
            ArgsImageFormat::Tiff => ril::ImageFormat::Tiff,
            ArgsImageFormat::WebP => ril::ImageFormat::WebP,
        }
    }
}

impl ArgsTextPosition {
    pub fn to_pos(&self, size: (u32, u32), padding: u32) -> (u32, u32) {
        let (w, h) = size;
        match self {
            ArgsTextPosition::Top => (w / 2, padding),
            ArgsTextPosition::TopLeft => (padding, padding),
            ArgsTextPosition::TopRight => (w - padding, padding),
            ArgsTextPosition::Center => (w / 2, h / 2),
            ArgsTextPosition::CenterLeft => (padding, h / 2),
            ArgsTextPosition::CenterRight => (w - padding, h / 2),
            ArgsTextPosition::Bottom => (w / 2, h - padding),
            ArgsTextPosition::BottomLeft => (padding, h - padding),
            ArgsTextPosition::BottomRight => (w - padding, h - padding),
        }
    }
}
