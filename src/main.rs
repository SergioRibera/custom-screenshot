use std::time::Instant;

use args::get_args;
use font_kit::family_name::FamilyName;
use font_kit::handle::Handle;
use font_kit::properties::Properties;
use font_kit::source::SystemSource;
use ril::{BitPixel, Ellipse, Font, Image, OverlayMode, Rectangle, Rgba, TextLayout, TextSegment};

mod args;
mod blur;

fn main() -> ril::Result<()> {
    let time = Instant::now();
    let args = get_args();
    let bg_size = if args.bg_size > args.shadow_size {
        args.bg_size
    } else {
        args.shadow_size
    };

    let mut out_dir = args.out_dir.unwrap();
    out_dir.push(args.input_file.file_name().unwrap());

    let img: Image<Rgba> = Image::open(args.input_file)?;
    let width = img.width() + bg_size * 2;
    let height = img.height() + bg_size * 2;

    println!(
        "Image Data\nwidth: {}\nheight: {}\nOutput Width: {width}\nOutput Height: {height}",
        img.width(),
        img.height()
    );

    // Rounded
    let mask = create_rounded_mask(img.width(), img.height(), args.corner_radius);

    let mut res: Image<Rgba> = Image::new(width, height, Rgba::from_hex(args.bg_color.as_str())?).with_overlay_mode(OverlayMode::Replace);

    if args.shadow {
        panic!("Shadow currently not works");
        // if let Some(shadow_color) = args.shadow_color {
        //     let shadow_color = Rgba::from_hex(shadow_color.as_str())?;
        //     let mut image = Image::new(width / 2, height / 2, Rgba::transparent());
        //     image.draw(
        //         &Rectangle::new()
        //             .with_position(image.width() / 2, image.height() / 2)
        //             .with_size(image.width(), image.height())
        //             .with_fill(shadow_color),
        //     );
        //     res.paste(
        //         0,
        //         0,
        //         &create_blur(&image, width, height, args.shadow_radius),
        //     );
        // }
        // else {
        //     let mut img = img.clone();
        //     img.resize(
        //         width / 4,
        //         height / 4,
        //         ril::ResizeAlgorithm::Bilinear,
        //     );
        //     let mut image = Image::new(width / 2, height / 2, Rgba::transparent());
        //     image.paste((width / 2) - image.width() / 3, (height / 2) - image.height() / 3, &img);
        //     res.paste(
        //         0,
        //         0,
        //         &create_blur(
        //             &img,
        //             width,
        //             height,
        //             args.shadow_radius,
        //         ),
        //     );
        // }
    }

    res.paste_with_mask(bg_size, bg_size, &img, &mask);

    if let Some(text) = args.text {
        if let Handle::Path { path, .. } = SystemSource::new()
            .select_best_match(&[FamilyName::Title(args.font)], &Properties::default())
            .expect("FontFamily not found")
        {
            let (x, y) = args
                .text_position
                .to_pos((width, height), (args.font_size as u32) + args.text_padding);
            let font = Font::open(path, args.font_size)?;
            let text = TextLayout::new()
                .centered()
                .with_position(x, y)
                .with_segment(&TextSegment::new(
                    &font,
                    text,
                    Rgba::from_hex(args.fg_color.as_str())?,
                ));
            res.draw(&text);
        } else {
            panic!("Failed to load font");
        }
    }

    println!("Time: {:?}", time.elapsed());

    res.save(args.format_image.into(), out_dir)
}

fn create_rounded_mask(width: u32, height: u32, radius: u32) -> Image<BitPixel> {
    let topleft = Ellipse::circle(radius, radius, radius).with_fill(BitPixel::on());
    let topright = Ellipse::circle(width - radius, radius, radius).with_fill(BitPixel::on());
    let bottomleft = Ellipse::circle(radius, height - radius, radius).with_fill(BitPixel::on());
    let bottomright =
        Ellipse::circle(width - radius, height - radius, radius).with_fill(BitPixel::on());

    let main_box = Rectangle::from_bounding_box(radius, radius, width - radius, height - radius)
        .with_fill(BitPixel::on());

    let top = Rectangle::new()
        .with_position(radius, 0)
        .with_size(width - radius * 2, radius)
        .with_fill(BitPixel::on());
    let bottom = Rectangle::new()
        .with_position(radius, height - radius)
        .with_size(width - radius * 2, radius)
        .with_fill(BitPixel::on());
    let left = Rectangle::new()
        .with_position(0, radius)
        .with_size(radius, height - radius * 2)
        .with_fill(BitPixel::on());
    let right = Rectangle::new()
        .with_position(width - radius, radius)
        .with_size(radius, height - radius * 2)
        .with_fill(BitPixel::on());

    let mut mask = Image::new(width, height, BitPixel::off());
    mask.draw(&topleft);
    mask.draw(&topright);
    mask.draw(&bottomleft);
    mask.draw(&bottomright);

    mask.draw(&main_box);
    mask.draw(&top);
    mask.draw(&bottom);
    mask.draw(&left);
    mask.draw(&right);

    mask
}
